/*
 * Copyright (c) Yangbajing 2018
 *
 * This is the custom License of Yangbajing
 */

package mass.core

object Constants {
  val BASE_CONF = "massdata"

  object Roles {
    val BROKER = "broker"
    val CONSOLE = "console"
  }

  object Nodes {
    val BROKER_LEADER = "broker-leader"

    val BROKER_LEADER_PROXY = "broker-leader-proxy"

    val BROKER = "mass-broker"

    val CONSOLE = "mass-console"
  }

}
